import axios from 'axios';
import { useCallback } from 'react';
import { useQuery, useQueryClient } from 'react-query';
import type { Blend } from './backend/blends/data';
import type { Spice } from './backend/spices/data';

const fetchAllBlends = () =>
  axios.get<Blend[]>('/api/v1/blends').then(({ data }) => data);

const fetchAllSpices = () =>
  axios.get<Spice[]>('/api/v1/spices').then(({ data }) => data);

const fetchBlend = (id: string) =>
  axios.get<Blend>(`/api/v1/blends/${id}`).then(({ data }) => data);

const fetchSpice = (id: string) =>
  axios.get<Spice>(`/api/v1/spices/${id}`).then(({ data }) => data);

/**
 * Retrieve all blends, but _not_ "full fat" data suitable for seed.
 */
export function useAllBlends() {
  return useQuery('blends', fetchAllBlends);
}

/**
 * Retrieve all spices, seeding structural cache for spice detail.
 */
export function useAllSpices() {
  return useQuery('spices', fetchAllSpices);
}

/**
 * Retrieve blend by id. We cannot use cached seed from allBlends,
 * as that data is missing the fancy associate serialization.
 *
 * To keep the load snappy, we prefetch blends from the home page.
 */
export function useBlendQuery(id: string) {
  const query = useQuery(['blends', id], () => fetchBlend(id));

  return query;
}

/**
 * Returns a factory suitable for creating stable onMouseOver callbacks.
 */
export function useBlendPrefetch() {
  const client = useQueryClient();

  return useCallback(
    (id: string) => () => {
      client.prefetchQuery(['blends', id], () => fetchBlend(id));
    },
    [client]
  );
}

/**
 * Retrieve spice record by id, using seed from allSpices cache if present.
 */
export function useSpiceQuery(id: string) {
  const client = useQueryClient();
  const query = useQuery(['spices', id], () => fetchSpice(id), {
    initialData: () =>
      client.getQueryData<Spice[]>('spices')?.find((spice) => spice.id === id),
  });

  return query;
}
