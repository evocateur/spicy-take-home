import type { RouteHandler } from 'miragejs/server';
import type { Registry } from 'miragejs';
import {
  createServer,
  Factory,
  Model,
  hasMany,
  RestSerializer,
} from 'miragejs';

//routes
import spiceRoutes from './spices/routes';
import blendRoutes from './blends/routes';

//data
import spices, { Spice } from './spices/data';
import blends, { Blend } from './blends/data';

const models = {
  spice: Model,
  blend: Model.extend({
    spices: hasMany(),
  }),
};

const factories = {
  spice: Factory.extend<Partial<Spice>>({
    name: (n: number) => `Spice ${n + 1}`,
    color: 'deadbeef',
    price: '$',
    heat: 0,
  }),
  blend: Factory.extend<Partial<Blend>>({
    name: (n: number) => `Blend ${n + 1}`,
    description: 'This is a new spice blend',
    blends: [],
  }),
};

export type SpicyRouteHandler = RouteHandler<
  Registry<typeof models, typeof factories>
>;

export function makeServer(options: {
  environment: typeof process.env.NODE_ENV;
}) {
  return createServer({
    environment: options.environment,

    models,

    factories,

    serializers: {
      application: RestSerializer.extend({
        embed: true,
        root: false,
      }),
    },

    fixtures: {
      spices,
      blends,
    },

    // istanbul ignore next (test mode doesn't call this)
    seeds(server) {
      server.loadFixtures();
    },

    routes() {
      this.namespace = '/api';

      //spices
      this.get('/v1/spices', spiceRoutes.getSpices);
      this.get('/v1/spices/2', spiceRoutes.errorSpice);
      this.get('/v1/spices/:id', spiceRoutes.getSpice);
      this.put('/v1/spices/:id', spiceRoutes.updateSpice);

      //blends
      this.get('/v1/blends', blendRoutes.getBlends);
      this.get('/v1/blends/:id', blendRoutes.getBlend);
      this.post('/v1/blends', blendRoutes.addBlend);
    },
  });
}
