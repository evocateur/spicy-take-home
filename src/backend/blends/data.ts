import type { Spice } from '../spices/data';

export interface Blend {
  id: string;
  name: string;
  description: string;
  blends: Blend[];
  spices: Spice[];
}

interface BlendFixture {
  id: number;
  name: string;
  blends: number[];
  // name necessary for mirage hasMany() association
  spiceIds: number[];
  description: string;
}

export default [
  {
    id: 0,
    name: 'Tasty Blend',
    blends: [],
    spiceIds: [1, 5, 35, 52],
    description: 'This is a new spice blend',
  },
  {
    id: 1,
    name: 'Blendy Blend',
    blends: [0],
    spiceIds: [2, 6, 37, 246],
    description: 'This is a new spice blend',
  },
  {
    id: 2,
    name: 'Meta Blend',
    blends: [0, 1],
    spiceIds: [400, 401, 402, 403],
    description: 'This is a new spice blend',
  },
] as BlendFixture[];
