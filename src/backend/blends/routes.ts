import { Response } from 'miragejs';
import { SpicyRouteHandler } from '..';

const blendRoutes: Record<string, SpicyRouteHandler> = {
  getBlends(schema, request) {
    const blends = schema.db.blends;
    return blends;
  },
  getBlend(this: FunctionRouteHandler, schema, request) {
    const { id } = request.params;
    const blend = schema.find('blend', id);

    if (!blend) {
      return new Response(404, {}, { errors: ['Blend not found.'] });
    }

    // context has magic methods, yayyyyyyy
    return serializeBlend.call(this, blend);
  },
  addBlend(schema, request) {
    const body = JSON.parse(request.requestBody);
    // add new blend
    const blendRes = schema.create('blend', body);
    return blendRes;
  },
};

export default blendRoutes;

/**
 * @see https://github.com/miragejs/miragejs/issues/757#issuecomment-764858124
 */
interface FunctionRouteHandler {
  schema: Parameters<SpicyRouteHandler>['0'];
  serialize: (response: unknown) => Record<string, unknown>;
}

// @ts-expect-error miragejs types are garbage
function serializeBlend(this: FunctionRouteHandler, blend) {
  const json = this.serialize(blend);

  const nestedBlends = this.schema.find('blend', blend.blends);

  if (nestedBlends) {
    // @ts-expect-error miragejs types are garbage (intentional arrow)
    json.blends = nestedBlends.models.reduce((arr, nestedBlend) => {
      arr.push(serializeBlend.call(this, nestedBlend));

      return arr;
    }, []);
  }

  json.spices = this.serialize(blend.spices);

  return json;
}
