import { screen } from '@testing-library/react';
import { makeServer } from '../../backend';
import { visit } from '../../test-helpers';

let server: ReturnType<typeof makeServer>;

beforeEach(() => {
  server = makeServer({ environment: 'test' });
});

afterEach(() => {
  server.shutdown();
});

test('renders blend detail page', async () => {
  server.create('blend', {
    spices: server.createList('spice', 2),
  });

  visit('/blends/1');

  expect(
    screen.getByRole('heading', { name: 'Blend Detail Page' })
  ).toBeInTheDocument();
  expect(await screen.findByText('Blend Name: Blend 1')).toBeInTheDocument();
  expect(
    screen.getByText('Spices Included:', { exact: false })
  ).toBeInTheDocument();
  expect(screen.getByRole('link', { name: 'Spice 2' })).toBeInTheDocument();
});

test('renders nested blends', async () => {
  const blend1 = server.create('blend', {
    spices: server.createList('spice', 3),
  });
  const blend2 = server.create('blend', {
    spices: server.createList('spice', 1),
  });

  // @ts-expect-error hack around reflexive many-to-many awkwardness
  blend2.blends = [blend1.id];

  visit('/blends/2');

  expect(await screen.findByText('Blend Name: Blend 2')).toBeInTheDocument();
  expect(screen.getByRole('link', { name: 'Spice 3' })).toBeInTheDocument();
  expect(screen.getByRole('link', { name: 'Spice 4' })).toBeInTheDocument();
});
