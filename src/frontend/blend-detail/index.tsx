import { useParams, Link } from 'react-router-dom';
import { useBlendQuery } from '../../fetchers';

export default function BlendDetail() {
  const { id } = useParams<{ id: string }>();
  // we cannot use cached blends data because it is incomplete
  // that's okay, we can prefetch blends from the home page.
  const query = useBlendQuery(id);

  return (
    <div>
      <h2>Blend Detail Page</h2>

      {query.isLoading ? (
        <>fetching</>
      ) : query.isError ? (
        // FIXME: reach into AxiosError and pull out `errors` from backend
        <p role="alert">{String(query.error)}</p>
      ) : query.isSuccess ? (
        <ul>
          <li>Blend Name: {query.data.name}</li>
          <li>Blend Description: {query.data.description}</li>
          {query.data.blends.length ? (
            <li>
              Blends Included:
              <ul>
                {query.data.blends.map((nestedBlend) => (
                  <li key={nestedBlend.id}>
                    <Link to={`/blends/${nestedBlend.id}`}>
                      {nestedBlend.name}
                    </Link>

                    <ul>
                      {nestedBlend.spices.map((spice) => (
                        <li key={spice.id}>
                          <Link to={`/spices/${spice.id}`}>{spice.name}</Link>
                        </li>
                      ))}
                    </ul>
                  </li>
                ))}
              </ul>
            </li>
          ) : null}
          <li>
            Spices Included:
            <ul>
              {query.data.spices.map((spice) => (
                <li key={spice.id}>
                  <Link to={`/spices/${spice.id}`}>{spice.name}</Link>
                </li>
              ))}
            </ul>
          </li>
        </ul>
      ) : null}
      <p>
        <Link to="/">Home</Link>
      </p>
    </div>
  );
}
