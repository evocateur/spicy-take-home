import { Link, useHistory } from 'react-router-dom';
import { useAllBlends, useAllSpices } from '../../fetchers';

export default function BlendCreate() {
  const history = useHistory();
  const allSpices = useAllSpices();
  const allBlends = useAllBlends();

  return (
    <div>
      <h2>Create Blend</h2>
      <form>
        <p>
          <label>
            Name
            <br />
            <input type="text" name="name" />
          </label>
        </p>
        <p>
          <label>
            Description
            <br />
            <input type="text" name="description" />
          </label>
        </p>
        <p>
          <button type="submit">Create</button>
          <button
            onClick={() => {
              history.push('/');
            }}
          >
            goan
          </button>
        </p>
      </form>
      <p>
        <Link to="/">Home</Link>
      </p>
    </div>
  );
}
