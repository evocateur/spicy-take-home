import axios from 'axios';
import { useMutation, useQueryClient } from 'react-query';
import { Link, useHistory } from 'react-router-dom';
import type { Blend } from '../backend/blends/data';
import { useAllBlends, useAllSpices } from '../fetchers';

type Payload = {
  name: string;
  description?: string;
  blends: string[];
  spiceIds: string[];
};

export default function BlendCreate() {
  const history = useHistory();
  const allSpices = useAllSpices();
  const allBlends = useAllBlends();

  const client = useQueryClient();
  const createBlend = useMutation(
    (payload: Payload) =>
      axios.post<Blend>('/api/v1/blends', payload).then(({ data }) => data),
    {
      onSuccess: () => {
        client.invalidateQueries('blends');
      },
    }
  );

  return (
    <div>
      <h2>Create Blend</h2>
      <form
        onSubmit={(e) => {
          e.preventDefault();

          const data = new FormData(e.currentTarget);

          // FIXME: Actual form validation would be nice
          const name = data.get('name') as string;
          const description = (data.get('description') as string) || undefined;
          const blends = data.getAll('blends') as string[];
          const spiceIds = data.getAll('spiceIds') as string[];

          const payload = {
            name,
            description,
            blends,
            spiceIds,
          };

          createBlend.mutate(payload, {
            onSuccess: (newBlend) => {
              history.push(`/blends/${newBlend.id}`);
            },
          });
        }}
      >
        <p>
          <label>
            Name
            <br />
            <input type="text" name="name" />
          </label>
        </p>
        <p>
          <label>
            Description
            <br />
            <input type="text" name="description" />
          </label>
        </p>
        <p>
          <label>
            Blends
            <br />
            <select multiple name="blends">
              {' '}
              {allBlends.isSuccess ? (
                allBlends.data.map((blend) => (
                  <option key={blend.id} value={blend.id}>
                    {blend.name}
                  </option>
                ))
              ) : (
                <option>Loading...</option>
              )}
            </select>
          </label>
        </p>
        <p>
          <label>
            Spices
            <br />
            <select multiple name="spiceIds">
              {allSpices.isSuccess ? (
                allSpices.data.map((spice) => (
                  <option key={spice.id} value={spice.id}>
                    {spice.name}
                  </option>
                ))
              ) : (
                <option>Loading...</option>
              )}
            </select>
          </label>
        </p>
        <p>
          <button type="submit">Create</button>
        </p>
      </form>
      <p>
        <Link to="/">Home</Link>
      </p>
    </div>
  );
}
