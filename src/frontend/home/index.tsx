import './index.css';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { useAllBlends, useAllSpices, useBlendPrefetch } from '../../fetchers';

function Home() {
  const [searchString, updateSearchString] = useState('');
  const allSpices = useAllSpices();
  const allBlends = useAllBlends();
  const prefetchBlend = useBlendPrefetch();

  return (
    <div className="section-container">
      <section className="spices-section">
        <h4>Spice List</h4>
        <div className="spices-filter">
          <label htmlFor="search">Filter </label>
          <input
            id="search"
            className="filter-input"
            autoComplete="off"
            value={searchString}
            onChange={(e) => {
              updateSearchString(e.target.value);
            }}
          />
        </div>
        <main className="spices-main">
          {allSpices.isLoading ? (
            <div>loading spices</div>
          ) : allSpices.isError ? (
            <p>{String(allSpices.error)}</p>
          ) : allSpices.isSuccess ? (
            allSpices.data
              .filter((spice) =>
                spice.name.toLowerCase().includes(searchString.toLowerCase())
              )
              .map((spice) => (
                <div key={spice.id}>
                  <Link to={`/spices/${spice.id}`}>{spice.name}</Link>
                </div>
              ))
          ) : (
            <>wat</>
          )}
        </main>
      </section>
      <section className="blends-section">
        <h4>Blend List</h4>

        <div className="create-blend">
          <Link to="/create/blend">Add a new blend</Link>
        </div>

        {allBlends.isLoading ? (
          <div>loading blends</div>
        ) : allBlends.isError ? (
          <p>{String(allBlends.error)}</p>
        ) : allBlends.isSuccess ? (
          allBlends.data
            .filter((blend) =>
              blend.name.toLowerCase().includes(searchString.toLowerCase())
            )
            .map((blend) => (
              <div key={blend.id}>
                <Link
                  to={`/blends/${blend.id}`}
                  onMouseOver={prefetchBlend(blend.id)}
                >
                  {blend.name}
                </Link>
              </div>
            ))
        ) : (
          <>WAT</>
        )}
      </section>
    </div>
  );
}

export default Home;
