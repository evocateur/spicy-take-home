import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { makeServer } from '../../backend';
import { visit } from '../../test-helpers';

let server: ReturnType<typeof makeServer>;

beforeEach(() => {
  server = makeServer({ environment: 'test' });
});

afterEach(() => {
  server.shutdown();
});

test('renders home page', async () => {
  server.create('blend', {
    spices: server.createList('spice', 2),
  });

  visit('/');

  expect(
    screen.getByRole('heading', { name: 'Spice List' })
  ).toBeInTheDocument();
  expect(
    screen.getByRole('heading', { name: 'Blend List' })
  ).toBeInTheDocument();

  expect(
    await screen.findByRole('link', { name: 'Spice 2' })
  ).toBeInTheDocument();
  expect(
    await screen.findByRole('link', { name: 'Blend 1' })
  ).toBeInTheDocument();
});

test('detail pages link back to home', async () => {
  server.create('spice');
  server.create('blend');

  visit('/');

  // go to spice detail (seeded from index query)
  userEvent.click(await screen.findByRole('link', { name: 'Spice 1' }));

  expect(screen.getByText('Spice Name: Spice 1')).toBeInTheDocument();

  // go back home
  userEvent.click(screen.getByRole('link', { name: 'Home' }));

  // go to blend detail (uncached, because mouseover is difficult to trigger)
  userEvent.click(await screen.findByRole('link', { name: 'Blend 1' }));

  expect(await screen.findByText('Blend Name: Blend 1')).toBeInTheDocument();

  // go back home
  userEvent.click(screen.getByRole('link', { name: 'Home' }));

  expect(
    await screen.findByRole('textbox', { name: 'Filter' })
  ).toBeInTheDocument();
});

test('list filtering', async () => {
  server.createList('spice', 12);

  visit('/');

  expect(
    await screen.findByRole('link', { name: 'Spice 9' })
  ).toBeInTheDocument();

  userEvent.type(screen.getByRole('textbox'), '1');

  // 1, 10, 11, 12
  expect(screen.queryAllByRole('link', { name: /Spice/ })).toHaveLength(4);
});

test('blend creation happy path', async () => {
  server.createList('spice', 3);
  server.create('blend', {
    spices: server.createList('spice', 2),
  });

  visit('/');

  // wait for the spices to load so the cache is primed
  expect(
    await screen.findByRole('link', { name: 'Spice 5' })
  ).toBeInTheDocument();

  userEvent.click(screen.getByRole('link', { name: 'Add a new blend' }));

  expect(screen.getByRole('option', { name: 'Spice 5' })).toBeInTheDocument();

  // fill form with valid values
  userEvent.type(
    screen.getByRole('textbox', { name: 'Name' }),
    'My Awesome Mix'
  );
  userEvent.type(
    screen.getByRole('textbox', { name: 'Description' }),
    "It's an awesome mix, brah"
  );
  userEvent.selectOptions(
    screen.getByRole('listbox', { name: 'Blends' }),
    screen.getByRole('option', { name: 'Blend 1' })
  );
  userEvent.selectOptions(
    screen.getByRole('listbox', { name: 'Spices' }),
    screen.getAllByRole('option', { name: /Spice (1|2|3)/ })
  );

  // initiate the mutation
  userEvent.click(screen.getByRole('button'));

  expect(
    await screen.findByText('Blend Name: My Awesome Mix')
  ).toBeInTheDocument();

  expect(screen.getAllByRole('link', { name: /Spice \d/ })).toHaveLength(5);
  expect(screen.getByRole('link', { name: 'Blend 1' })).toBeInTheDocument();
});
