import { screen } from '@testing-library/react';
import { makeServer } from '../../backend';
import { visit } from '../../test-helpers';

let server: ReturnType<typeof makeServer>;

beforeEach(() => {
  server = makeServer({ environment: 'test' });
});

afterEach(() => {
  server.shutdown();
});

test('renders spice detail page', async () => {
  server.create('spice');

  visit('/spices/1');

  expect(
    screen.getByRole('heading', { name: 'Spice Detail Page' })
  ).toBeInTheDocument();
  expect(await screen.findByText('Spice Name: Spice 1')).toBeInTheDocument();
});
