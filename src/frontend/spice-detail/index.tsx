import { Link, useParams } from 'react-router-dom';
import { useSpiceQuery } from '../../fetchers';

const SpiceDetail = () => {
  const { id } = useParams<{ id: string }>();
  const query = useSpiceQuery(id);

  return (
    <div>
      <h2>Spice Detail Page</h2>
      {query.isLoading ? (
        <>fetching</>
      ) : query.isError ? (
        // FIXME: reach into AxiosError and pull out `errors` from backend
        <p role="alert">{String(query.error)}</p>
      ) : query.isSuccess ? (
        <ul>
          <li>Spice Name: {query.data.name}</li>
          <li>Spice Color: {query.data.color}</li>
          <li>Spice Cost: {query.data.price}</li>
          <li>Spice Heat Level: {query.data.heat}</li>
        </ul>
      ) : null}
      <p>
        <Link to="/">Home</Link>
      </p>
    </div>
  );
};

export default SpiceDetail;
