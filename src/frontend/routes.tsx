import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './home';
import SpiceDetail from './spice-detail';
import BlendDetail from './blend-detail';
import BlendCreate from './create-blend';

export default function App() {
  return (
    <React.StrictMode>
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/spices/:id">
          <SpiceDetail />
        </Route>
        <Route path="/blends/:id">
          <BlendDetail />
        </Route>
        <Route path="/create/blend">
          <BlendCreate />
        </Route>
      </Switch>
    </React.StrictMode>
  );
}
